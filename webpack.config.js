const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const proMode = !(devMode = process.env.NODE_ENV !== 'production'? true : false);

const entryPath = path.resolve('src', proMode? 'index.ts' : 'app.tsx');
const outputPath = path.resolve(__dirname, 'dist');
const htmlTemplatePath = path.resolve(__dirname, 'src', 'index.html');

module.exports = {
  mode: devMode? 'development' : 'production',
  devtool: proMode? 'inline-source-map' : false,
  entry: entryPath,
  output: {
    filename: 'index.js',
    path: outputPath,
    library: {
      type: 'umd',
    },
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
  },
  module: {
    rules: [
      {
        test: /\.(ts|tsx)$/,
        use: 'ts-loader',
      },
      {
        test: /\.s[ac]ss$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: true,
            },
          },
          'sass-loader'
        ],
      },
      {
        test: /\.svg$/,
        type: 'asset/inline',
      },
    ]
  },
  optimization: {
    minimize: proMode,
    minimizer: [new TerserPlugin(), ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: htmlTemplatePath,
    }),
  ],
  devServer: {
    port: 8080,
  }
}
