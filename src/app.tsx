import ReactDom from 'react-dom';
import Navbar from './Navbar';

ReactDom.render(
  <Navbar />,
  document.getElementById('app'),
)

