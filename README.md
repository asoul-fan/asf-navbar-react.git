# A-SOUL Fan 导航栏

该项目为 [asf-navbar-vue](https://gitee.com/asoul-fan/asf-navbar-vue) 的 `react` 翻译项目，基本上所有的功能开发都在 [asf-navbar-vue](https://gitee.com/asoul-fan/asf-navbar-vue) 中完成，该项目负责将语言实时翻译更新至 `react`。因此，本文只简述项目，具体开发指南详见 [asf-navbar-vue/README.md](https://gitee.com/asoul-fan/asf-navbar-vue/blob/master/README.md)。

## 安装

```bash
npm install asf-navbar-react
# Or with yarn
yarn add asf-navbar-react
```

## Usage

```typescript
import Navbar from 'asf-navbar-react';

export default () => {
  return (
    <div>
      <Navbar />
      <!-- Other codes -->
    </div>
  )
}
```

## 项目相关命令

```bash
# 安装依赖项
yarn install

# 预览导航栏效果
yarn serve

# 构建项目
# Production mode
yarn build
# Development mode
yarn build:dev

# 清除构建文件
yarn clean

# 测试项目？
# 根本没写测试！
```

## 贡献代码

你或许已经注意到了，所有的 `commit message` 都由 `emoji`😈 开头，这是因为项目使用了
`gitmoji` 为不同 `commit` 选择了不同的 `emoji` 作为标志。

因此，开发前请先安装并配置 `gitmoji`

```bash
yarn global add gitmoji-cli # npm/tyarn 同理
# 在项目根目录下
gitmoji --init
```

做做实验看看 `gitmoji` 和 `react` 怎么用的，然后就随便写写咯。
